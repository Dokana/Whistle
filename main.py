from time import sleep_ms
import machine
import network
from umqtt.simple import MQTTClient
from inputs import Digital, Manager

mqtt = None

def connect():  # connects to the WiFi
    wlan = network.WLAN(network.STA_IF)

    if not wlan.isconnected():
        wlan.active(True)
        wlan.connect('SSID', 'password')

        while not wlan.isconnected():
            sleep_ms(100)

    # connect to broker
    mqtt = MQTTClient('whistle', '192.168.0.20')
    mqtt.connect()

def pushed():    # triggered when button is pushed down
    mqtt.publish(b'whistle', b'blow')

def released():  # triggered when the button is released
    mqtt.publish(b'whistle', b'blow')

connect()
switch = machine.Pin(34, machine.Pin.IN, machine.Pin.PULL_UP)
mgr = Manager([Digital('34:switch', hl_func=released, lh_func=pushed)])

while True:  # this is only here to stop this program from exiting
    sleep_ms(100)
