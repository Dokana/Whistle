# Whistle

<img src="whistle.jpg" align="left" width="200px"/>
A wireless, handheld switch that broadcasts a message to other devices; who in
turn choose how they are going to respond.
<br clear="left"/>

## Architecture

We use [**MQTT**](https://www.hivemq.com/article/how-to-get-started-with-mqtt/), a
**protocol** (a set of rules and conventions) that was designed to provide a
lightweight, efficient, and reliable messaging protocol for facilitating
communication between sensors, and control systems in industrial environments.
One of the earliest applications of MQTT was in monitoring and controlling oil
pipelines. It's now used widely in healthcare, energy, automotive and
agriculture industries.

![Architecture](architecture.png)


## Tools / Techniques

- [Pomodoro Technique](https://www.youtube.com/watch?v=mNBmG24djoY)
- [Vim](https://en.wikipedia.org/wiki/Vim_(text_editor))
- [MicroPython](https://en.wikipedia.org/wiki/MicroPython)
- [Markdown](https://en.wikipedia.org/wiki/Markdown)
- [Socratic Questioning](https://en.wikipedia.org/wiki/Socratic_questioning)
- [Man pages](https://en.wikipedia.org/wiki/Man_page)
- [MQTT](https://www.hivemq.com/article/how-to-get-started-with-mqtt/)
- [mqttwarn](https://mqttwarn.readthedocs.io/en/latest/)
- [Mosquitto](https://www.halukyamaner.com/what-is-mosquitto)
- [IoT](https://en.wikipedia.org/wiki/Internet_of_things)
- Bash, Git


## Shopping List

### Materials

- [ESP32 S2 Mini](https://www.aliexpress.com/item/1005006157693055.html) **£1.63**
- [3.7V 320mAh battery](https://www.amazon.co.uk/dp/B08215N9R8) **£5.99**
- [TP4056 charging module](https://www.aliexpress.com/item/1005004432351925.html) **£0.46**
- [10mm polymide tape](https://www.aliexpress.com/item/1005005058190496.html) **£0.99**
- [micro switch](https://www.aliexpress.com/item/1005005678108657.html) **£0.95**


### Tools

- [soldering iron](https://www.aliexpress.com/item/1005004458912712.html) **£29.75**
- [soldering mat](https://www.aliexpress.com/item/1005005016195025.html) **£15.92**
- [solder wire](https://www.aliexpress.com/item/1005005506183836.html) **£3.64**


## TODO

- [ ] increase battery size to [3.7V 1800mAh](https://www.amazon.co.uk/dp/B08ZCQXFX4)
- [ ] sliding `on/off` switch
- [ ] improve push button switch, [see here](https://www.aliexpress.com/item/1005005382366527.html)
- [ ] systemd units to automatically start `mqttwarn` on laptop boot
- [ ] case
    - [ ] Zippo case or case holder, [see here](https://www.amazon.co.uk/dp/B0BYB6FR94/)
    - [ ] [epoxy resin](https://www.amazon.co.uk/dp/B0B24W3P3H/)
    - [ ] wooden carved box
    - [ ] two boards separated with [spacers](https://www.amazon.co.uk/dp/B07TKV61PC/)
